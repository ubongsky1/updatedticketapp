﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HelpDesk.Data;
using HelpDesk.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using MimeKit;
using MimeKit.Text;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace HelpDesk.Controllers
{
    [Authorize]
    public class TicketsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public TicketsController(ApplicationDbContext context,
                                SignInManager<ApplicationUser> signInManager,
                                UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: Tickets
        [Authorize(Roles = "Users")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var applicationDbContext = await _context.Tickets.OrderByDescending(x => x.Id).Where(x=>x.AddedBy==user.Email).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(applicationDbContext);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> OpenAdmin()
        {
            var user = await _userManager.GetUserAsync(User);
            var app = await _context.Tickets.OrderByDescending(x => x.Id).Where(x=>x.isTreated == false).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(app);
        }

        public  IActionResult GetData()
        {
            int total = _context.Tickets.Count();
            int approved = _context.Tickets.Where(x => x.isTreated).Count();
            int disapproved = _context.Tickets.Where(x => x.notTreated).Count();
            int pending = _context.Tickets.Where(x => x.isTreated == false).Count();

            Ratio obj = new Ratio
            {
                Total = total,
                Approved = approved,
                Disapproved = disapproved,
                Pending=pending
            };

            return Json(obj, new Newtonsoft.Json.JsonSerializerSettings());
        }




        public class Ratio
        {
            public int Total { get; set; }
            public int Approved { get; set; }
            public int Disapproved { get; set; }
            public int Pending { get; set; }
        }

        public IActionResult About()
        {
           
            return View();
        }



        [Authorize(Roles = "Staff")]
        public async Task<IActionResult> OpenAssignee()
        {
            var user = await _userManager.GetUserAsync(User);
            var app = await _context.Tickets.OrderByDescending(x => x.Id).Where(x => x.AssignToId == user.Id & x.isTreated == false).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(app);
        }

        [Authorize(Roles = "Users")]
        public async Task<IActionResult> Open()
        {
            var user = await _userManager.GetUserAsync(User);
            var app = await _context.Tickets.OrderByDescending(x => x.Id).Where(x => x.AddedBy == user.Email & x.isTreated == false).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(app);
        }


        [Authorize(Roles = "Staff")]
        public async Task<IActionResult> Assignee()
        {
            var user = await _userManager.GetUserAsync(User);
            var app = await _context.Tickets.OrderByDescending(x => x.Id).Where(x => x.AssignToId == user.Id).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(app);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> TicketAdmin()
        {
            var user = await _userManager.GetUserAsync(User);
            var applicationDbContext = await _context.Tickets.OrderByDescending(x => x.Id).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(applicationDbContext);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Reports()
        {
            var user = await _userManager.GetUserAsync(User);
            var applicationDbContext = await _context.Tickets.OrderByDescending(x => x.Id).Include(t => t.ApplicationUser).Include(t => t.Units).ToListAsync();
            return View(applicationDbContext);
        }

        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets
                .Include(t => t.ApplicationUser)
                .Include(t => t.Units)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

      
        // GET: Tickets/Create
        public IActionResult Create()
        {
            var test = (from a in _context.Users
                        join b in _context.UserRoles on a.Id equals b.UserId
                        join c in _context.Roles on b.RoleId equals c.Id
                        where a.Id == b.UserId && c.Name != "Users"
                        select new
                        {
                            Value = b.UserId,
                            Text = a.FirstName + ' ' + a.LastName
                        }).ToList();
            ViewData["AssignToId"] = new SelectList(test, "Value", "Text");
            //ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "UserName");
            ViewData["UserUnit"] = new SelectList(_context.Units, "Id", "Name");
            return PartialView();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Create([Bind("Id,UserId,Subject,Content,Status,Response,Priority,TreatedBy,IsDeleted,isTreated,notTreated,AddedBy,AssignToId,UserUnit,CreateDate")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ticket);
                await _context.SaveChangesAsync();

                var user = await _userManager.GetUserAsync(User);
                var check = _context.Users.Where(x => x.Id == ticket.AssignToId).Select(s => new { s.Email }).FirstOrDefault();
                var sample = check.Email;

                 var message = new MimeMessage();
                //Setting the To e-mail address
                message.To.Add(new MailboxAddress(sample));

                var ss = user.Email;
                var tt = "ubongsky1@gmail.com";
                message.Cc.Add(new MailboxAddress(ss));
                //Setting the From e-mail address
                message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                //E-mail subject 
                message.Subject = "Help Desk Ticket";
                //E-mail message body
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                   "<br/><br/><br/><br/>" +
                "<p>Dear Colleague,</p>" +

               "<p> This is to notify that a ticket has been created by " + user.FirstName + " " + user.LastName +

               "<br/>on </a>" + ticket.CreateDate + ".</p>" +
               "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + ticket.Id + "'>Here</a>" +




                                              "<br/>Regards<br/>" +


                                               "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                };

                //Configure the e-mail
                using (var emailClient = new SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Connect("smtp.office365.com", 587, false);
                    emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2009aa");
                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }

                if (_signInManager.IsSignedIn(User) && User.IsInRole("Admin"))
                {
                    return RedirectToAction("TicketAdmin", "Tickets");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("Staff"))
                {
                    return RedirectToAction("Assignee", "Tickets");
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "UserName", ticket.AssignToId);
            ViewData["UserUnit"] = new SelectList(_context.Units, "Id", "Name", ticket.UserUnit);
            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets.FindAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }
            var test = (from a in _context.Users
                        join b in _context.UserRoles on a.Id equals b.UserId
                        join c in _context.Roles on b.RoleId equals c.Id
                        where a.Id == b.UserId && c.Name != "Users"
                        select new
                        {
                            Value = b.UserId,
                            Text = a.FirstName + ' ' + a.LastName
                        }).ToList();
            ViewData["AssignToId"] = new SelectList(test, "Value", "Text", ticket.AssignToId);
            //ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "UserName", ticket.AssignToId);
            ViewData["UserUnit"] = new SelectList(_context.Units, "Id", "Name", ticket.UserUnit);
            return PartialView(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Edit(Ticket ticket)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ticket);
                    await _context.SaveChangesAsync();

                    if (ticket.isTreated==true && ticket.notTreated==false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var check = _context.Users.Where(x => x.Id == ticket.AssignToId).FirstOrDefault();
                        var simp = _context.Users.Where(x => x.UserName == ticket.AddedBy).Select(s => new { s.Email }).FirstOrDefault();
                        var sample = simp.Email;

                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));

                        var ss = user.Email;
                        var tt = "ubongsky1@gmail.com";
                        message.Bcc.Add(new MailboxAddress(ss));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "Help Desk Ticket";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify that the ticket has been solved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + ticket.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + ticket.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2009aa");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (ticket.notTreated==true && ticket.isTreated==false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var check = _context.Users.Where(x => x.Id == ticket.AssignToId).FirstOrDefault();
                        var simp = _context.Users.Where(x => x.UserName == ticket.AddedBy).Select(s => new { s.Email }).FirstOrDefault();
                        var sample = simp.Email;

                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));

                        var ss = user.Email;
                        var tt = "ubongsky1@gmail.com";
                        message.Bcc.Add(new MailboxAddress(ss));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "Help Desk Ticket";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify that the ticket has been rejected by " + check.FirstName + " " + check.LastName +

                       "<br/>on </a>" + ticket.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + ticket.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2009aa");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TicketExists(ticket.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                if (_signInManager.IsSignedIn(User) && User.IsInRole("Admin"))
                {
                    return RedirectToAction("TicketAdmin", "Tickets");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("Staff"))
                {
                    return RedirectToAction("Assignee", "Tickets");
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "UserName", ticket.AssignToId);
            ViewData["UserUnit"] = new SelectList(_context.Units, "Id", "Name", ticket.UserUnit);
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets
                .Include(t => t.ApplicationUser)
                .Include(t => t.Units)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ticket = await _context.Tickets.FindAsync(id);
            _context.Tickets.Remove(ticket);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TicketExists(int id)
        {
            return _context.Tickets.Any(e => e.Id == id);
        }
    }
}
