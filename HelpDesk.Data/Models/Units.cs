﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HelpDesk.Data.Models
{
    public class Units
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [DisplayName("Added By")]
        public string AddedBy { get; set; }
        [DisplayName("Added On")]
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}