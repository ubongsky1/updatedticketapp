﻿using Microsoft.AspNetCore.Identity;

namespace HelpDesk.Data.Models
{
    public class RoleClaim : IdentityRoleClaim<int>
    {
        public virtual ApplicationRole Role { get; set; }
    }
}