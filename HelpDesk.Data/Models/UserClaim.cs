﻿using Microsoft.AspNetCore.Identity;

namespace HelpDesk.Data.Models
{
    public class UserClaim : IdentityUserClaim<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}