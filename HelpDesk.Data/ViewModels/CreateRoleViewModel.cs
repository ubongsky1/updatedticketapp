﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HelpDesk.Data.ViewModels
{
   public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; }
    }
}
